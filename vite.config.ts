import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import { resolve } from "path";
import eslint from "vite-plugin-eslint";
import tsconfigPaths from "vite-tsconfig-paths";
import AutoImport from "unplugin-auto-import/vite";
import Components from "unplugin-vue-components/vite";
import { ElementPlusResolver } from "unplugin-vue-components/resolvers";

// https://vitejs.dev/config/
export default defineConfig({
  base: "./",
  plugins: [
    tsconfigPaths(),
    vue(),
    eslint({
      cache: false,
      fix: false,
      include: ["src/**/*.ts", "src/**/*.vue"]
    }),
    AutoImport({
      resolvers: [ElementPlusResolver()]
    }),
    Components({
      resolvers: [ElementPlusResolver()]
    })
  ],
  resolve: {
    alias: {
      "@": resolve(__dirname, "./src")
    }
  },
  build: {
    chunkSizeWarningLimit: 1024,
    rollupOptions: {
      output: {
        chunkFileNames: "static/js/[name]-[hash].js",
        entryFileNames: "static/js/[name]-[hash].js",
        assetFileNames: "static/[ext]/[name]-[hash].[ext]",
        manualChunks: {
          // elementPlus: ["element-plus"],
          vue: ["vue"],
          vueRouter: ["vue-router"]
        }
        // manualChunks(id) {
        //   console.log(id)
        //   return ''
        //   // 将pinia的全局库实例打包进vendor，避免和页面一起打包造成资源重复引入
        //   // if (id.includes(path.resolve(__dirname, '/src/store/index.ts'))) {
        //   //   return 'vendor';
        //   // }
        // }
      }
    }
  }
});
