import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "Home",
    component: () => import("@/Home.vue")
  },
  {
    path: "/login",
    name: "Login",
    component: () => import("@/Login.vue")
  },
  {
    path: "/reactive",
    name: "Reactive",
    component: () => import("@/views/reactive/index.vue")
  },
  {
    path: "/echarts",
    name: "Echarts",
    component: () => import("@/views/echarts/index.vue")
  },
  {
    path: "/directives",
    name: "Directives",
    component: () => import("@/views/directives/index.vue")
  },
  {
    path: "/treeSelect",
    name: "TreeSelect",
    component: () => import("@/views/treeSelect/index.vue")
  },
  {
    path: "/qrcode",
    name: "QRCode",
    component: () => import("@/views/qrcode/index.vue")
  },
  {
    path: "/qrcode2",
    name: "QRCode2",
    component: () => import("@/views/qrcode/qrcode2.vue")
  }
];

const router = createRouter({
  history: createWebHistory("/"),
  routes
});

export default router;
